#!/bin/bash

echo -e "[batch:children]\n" >> test/hosts
echo -e "[rp:children]\n" >> test/hosts
echo -e "[jboss:children]\n" >> test/hosts
echo -e "[pgsql:children]\n" >> test/hosts
echo -e "[syslog:children]\n" >> test/hosts

echo -e "[batch:children]\n" >> prep/hosts
echo -e "[rp:children]\n" >> prep/hosts
echo -e "[jboss:children]\n" >> prep/hosts
echo -e "[pgsql:children]\n" >> prep/hosts
echo -e "[syslog:children]\n" >> prep/hosts

echo -e "[batch:children]\n" >> prod/hosts
echo -e "[rp:children]\n" >> prod/hosts
echo -e "[jboss:children]\n" >> prod/hosts
echo -e "[pgsql:children]\n" >> prod/hosts
echo -e "[syslog:children]\n" >> prod/hosts


for host in `ls noc/hosts-*`; do
  env=$(echo $host | cut -d '-' -f2)
  prest=$(echo $host | cut -d '-' -f3)

  #Agrego como comentario el nombre del prestador
  echo -e "\n####${prest}####\n" >> $env/hosts

  #Muevo las variables a group_vars
  tail -n +2 $host | grep -oPz "(?s)[^\n]*${s}.*?\n.*?children.*?\n" | head -n -2 > $env/group_vars/$prest

  #Borro las lineas vacias
  sed -i '/^$/d' $env/group_vars/$prest

  # Reemplazdo = por :
  sed -i "s/=/: /" $env/group_vars/$prest

  sed -n -e '/children/,$p' $host > $env/temp_host


  sed -i "s/syslog/syslog-$prest/" $env/temp_host

  grep syslog-$prest $env/temp_host
  
  if [ $? -eq 0 ]; then
    sed -i "/syslog:children/a syslog-$prest" $env/hosts
  fi


  sed -i "s/batch/batch-$prest/" $env/temp_host
  
  grep batch-$prest $env/temp_host
  if [ $? -eq 0 ]; then
    sed -i "/batch:/a batch-$prest" $env/hosts
  fi


  sed -i "s/rp/rp-$prest/" $env/temp_host
  grep rp-$prest $env/temp_host
  if [ $? -eq 0 ]; then
    sed -i "/rp:/a rp-$prest" $env/hosts
  fi

  sed -i "s/rp1/rp1-$prest/" $env/temp_host
  sed -i "s/rp2/rp2-$prest/" $env/temp_host

  sed -i "s/jboss/jboss-$prest/" $env/temp_host
  grep jboss-$prest $env/temp_host
  if [ $? -eq 0 ]; then
    sed -i "/jboss:/a jboss-$prest" $env/hosts
  fi

  sed -i "s/proxypge1/proxypge1-$prest/" $env/temp_host
  sed -i "s/proxypge2/proxypge2-$prest/" $env/temp_host

  sed -i "s/pgsql/pgsql-$prest/" $env/temp_host
  grep pgsql-$prest $env/temp_host
  if [ $? -eq 0 ]; then
    sed -i "/pgsql:/a pgsql-$prest" $env/hosts
  fi

  sed -i "s/proxypge-psql1/proxypge-psql1-$prest/" $env/temp_host
  sed -i "s/proxypge-psql2/proxypge-psql2-$prest/" $env/temp_host
  sed -i "s/icinga/icinga-$prest/" $env/temp_host
  grep icinga-$prest $env/temp_host
  if [ $? -eq 0 ]; then
    sed -i "s/$env:/$prest:/" $env/temp_host
  fi
  sed -i "s/preprod:/$prest:/" $env/temp_host
  sed -i "s/testcomp:/$prest:/" $env/temp_host


cat $env/temp_host >> $env/hosts
rm $env/temp_host

done
